package ar.edu.asap.practica00.modelo;

public class Spock extends PiedraPapelTijeraLagartoSpockFactory {
	
	public Spock() {
		this("spock", PiedraPapelTijeraLagartoSpockFactory.SPOCK);
	}

	public Spock(String nombre, int numero) {
		super(nombre, numero);

	}

	@Override
	public boolean isMe(int pNUm) {

		return pNUm == PiedraPapelTijeraLagartoSpockFactory.SPOCK;
	}

	@Override
	public int comparar(PiedraPapelTijeraLagartoSpockFactory pPPTFact) {
		int result = 0;

		switch (pPPTFact.getNumero()) {
		
		//SPOCK LE GANA A TIJERA
		case PiedraPapelTijeraLagartoSpockFactory.TIJERA:
			result = 1;
			this.descripcionResultado = nombre + " le gana a " + pPPTFact.getNombre();

			break;
			
		//SPOCK LE GANA A PIEDRA
			
		case PiedraPapelTijeraLagartoSpockFactory.PIEDRA:
			result = 2;
			this.descripcionResultado = nombre + " le gana a " + pPPTFact.getNombre();

			break;
		
		//SPOCK PIERDE CON PAPEL
		case PiedraPapelTijeraLagartoSpockFactory.PAPEL:
			result = -3;
			this.descripcionResultado = nombre + " pierde con " + pPPTFact.getNombre();

			break;
		//SPOCK PIERDE CON LAGARTO	
		case PiedraPapelTijeraLagartoSpockFactory.LAGARTO:
			result = -1;
			this.descripcionResultado = nombre + " pierde con " + pPPTFact.getNombre();

			break;
			
		//SPOCK EMPATA CON SPOCK
		default:

			this.descripcionResultado = nombre + " empata con " + pPPTFact.getNombre();

			break;

		}
		return result;
	}


}
