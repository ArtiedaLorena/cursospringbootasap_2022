package ar.edu.asap.practica00.modelo.test;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ar.edu.asap.practica00.modelo.Lagarto;
import ar.edu.asap.practica00.modelo.Papel;
import ar.edu.asap.practica00.modelo.Piedra;
import ar.edu.asap.practica00.modelo.PiedraPapelTijeraLagartoSpockFactory;
import ar.edu.asap.practica00.modelo.Spock;
import ar.edu.asap.practica00.modelo.Tijera;

class PiedraPapelTijeraFactoryTest {
	// Lote de pruebas

	Piedra piedra;
	Papel papel;
	Tijera tijera;
	Spock spock;
	Lagarto lagarto;

	@BeforeEach
	void setUp() throws Exception {
		//Se ejecuta antes de cada test
		piedra= new Piedra();
		papel= new Papel();
		tijera= new Tijera();
		spock= new Spock();
		lagarto= new Lagarto();
	}

	@AfterEach
	void tearDown() throws Exception {
		//Se destruye despeus de cada test
		piedra=null;
		papel=null;
		tijera= null;
		spock=null;
		lagarto=null;
	}
	
	@Test
	void testGetNumero() {
		assertEquals(1,piedra.getNumero());
	}

	//PIEDRA GANA/PIERDE/EMPATA
	@Test
	void testPiedraGanaTijera_comparar() {
		assertEquals(1, piedra.comparar(tijera));
	}

	@Test
	void testPiedraGanaLagarto_comparar() {
		assertEquals(2, piedra.comparar(lagarto));
	}



	@Test
	void testPiedraPierdePapel_comparar() {
		assertEquals(-3, piedra.comparar(papel));
	}

	@Test
	void testPiedraPierdeSpock_comparar() {
		assertEquals(-1, piedra.comparar(spock));
	}

	@Test
	void testPiedraEmpataPiedra_comparar() {
		assertEquals(0, piedra.comparar(piedra));
	}




	//PAPEL GANA/PIERDE/EMPATA

	@Test
	void testPapelGanaPiedra_comparar() {
		assertEquals(1, papel.comparar(piedra));
	}

	@Test
	void testPapelGanaSpock_comparar() {
		assertEquals(2, papel.comparar(spock));
	}


	@Test
	void testPapelPierdeTijera_comparar() {
		assertEquals(-3, papel.comparar(tijera));
	}

	@Test
	void testPapelPierdeLagarto_comparar() {
		assertEquals(-1, papel.comparar(lagarto));
	}

	@Test
	void testPapelEmpataPapel_comparar() {
		assertEquals(0, papel.comparar(papel));
	}


	//TIJERA GANA/PIERDE/EMPATA

	@Test
	void testTijeraGanaPapel_comparar() {
		assertEquals(1, tijera.comparar(papel));
	}



	@Test
	void testTijeraGanaLagarto_comparar() {
		assertEquals(2, tijera.comparar(lagarto));
	}

	@Test
	void testTijeraPierdePiedra_comparar() {
		assertEquals(-3, tijera.comparar(piedra));
	}

	@Test
	void testTijeraPierdeSpock_comparar() {
		assertEquals(-1,tijera.comparar(spock));
	}


	@Test
	void testTijeraEmpataTijera_comparar() {
		assertEquals(0, tijera.comparar(tijera));
	}

	//LAGARTO GANA/PIERDE/EMPATA

	@Test
	void testLagartoGanaPapel_comparar() {
		assertEquals(1, lagarto.comparar(papel));
	}



	@Test
	void testLagartoGanaSpock_comparar() {
		assertEquals(2, lagarto.comparar(spock));
	}

	@Test
	void testLagartoPierdePiedra_comparar() {
		assertEquals(-3, lagarto.comparar(piedra));
	}

	@Test
	void testLagartoPierdeTijera_comparar() {
		assertEquals(-1,lagarto.comparar(tijera));
	}


	@Test
	void testLagartoEmpataLagarto_comparar() {
		assertEquals(0, lagarto.comparar(lagarto));
	}

	//SPOCK GANA/PIERDE/EMPATA

	@Test
	void testSpockGanaTijera_comparar() {
		assertEquals(1, spock.comparar(tijera));
	}



	@Test
	void testSpockGanaPiedra_comparar() {
		assertEquals(2, spock.comparar(piedra));
	}

	@Test
	void testSpockPierdePapel_comparar() {
		assertEquals(-3, spock.comparar(papel));
	}

	@Test
	void testSpockPierdeLagarto_comparar() {
		assertEquals(-1,spock.comparar(lagarto));
	}


	@Test
	void testSpockEmpataSpock_comparar() {
		assertEquals(0, spock.comparar(spock));
	}


	//PIEDRA GANA/PIERDE/EMPATA COMPARA RESULTADO

	@Test
	void testPiedraGanaTijera_comparar_DescripcionResultado(){
		piedra.comparar(tijera);
		assertEquals("piedra le gana a tijera",piedra.getDescripcionResultado());
	}
	
	@Test
	void testPiedraGanaLagarto_comparar_DescripcionResultado(){
		piedra.comparar(lagarto);
		assertEquals("piedra le gana a lagarto",piedra.getDescripcionResultado());
	}

	@Test
	void testPiedraPierdePapel_comparar_DescripcionResultado(){
		piedra.comparar(papel);
		assertEquals("piedra pierde con papel",piedra.getDescripcionResultado());
	}
	
	@Test
	void testPiedraPierdeSpock_comparar_DescripcionResultado(){
		piedra.comparar(spock);
		assertEquals("piedra pierde con spock",piedra.getDescripcionResultado());
	}

	@Test
	void testPiedraEmpataPiedra_comparar_DescripcionResultado(){
		piedra.comparar(piedra);
		assertEquals("piedra empata con piedra",piedra.getDescripcionResultado());
	}
	
	
	//PAPEL GANA/EMPATA/PIERDE COMPARA RESULTADO
	
	@Test
	void testPapelGanaPiedra_comparar_DescripcionResultado(){
		papel.comparar(piedra);
		assertEquals("papel le gana a piedra",papel.getDescripcionResultado());
	}
	
	@Test
	void testPapelGanaSpock_comparar_DescripcionResultado(){
		papel.comparar(spock);
		assertEquals("papel le gana a spock",papel.getDescripcionResultado());
	}

	@Test
	void testPapelPierdeTijera_comparar_DescripcionResultado(){
		papel.comparar(tijera);
		assertEquals("papel pierde con tijera",papel.getDescripcionResultado());
	}
	
	@Test
	void testPapelPierdeLagarto_comparar_DescripcionResultado(){
		papel.comparar(lagarto);
		assertEquals("papel pierde con lagarto",papel.getDescripcionResultado());
	}

	@Test
	void testPapelEmpataPapel_comparar_DescripcionResultado(){
		papel.comparar(papel);
		assertEquals("papel empata con papel",papel.getDescripcionResultado());
	}
	

	//TIJERA GANA/PIERDE/EMPATA COMPARA RESULTADO

	@Test
	void testTijeraGanaPapel_comparar_DescripcionResultado(){
		tijera.comparar(papel);
		assertEquals("tijera le gana a papel",tijera.getDescripcionResultado());
	}
	
	@Test
	void testTijeraGanaLagarto_comparar_DescripcionResultado(){
		tijera.comparar(lagarto);
		assertEquals("tijera le gana a lagarto",tijera.getDescripcionResultado());
	}
	

	@Test
	void testTijeraPierdePiedra_comparar_DescripcionResultado(){
		tijera.comparar(piedra);
		assertEquals("tijera pierde con piedra",tijera.getDescripcionResultado());
	}
	
	@Test
	void testTijeraPierdeSpock_comparar_DescripcionResultado(){
		tijera.comparar(spock);
		assertEquals("tijera pierde con spock",tijera.getDescripcionResultado());
	}

	@Test
	void testTijeraEmpataTijera_comparar_DescripcionResultado(){
		tijera.comparar(tijera);
		assertEquals("tijera empata con tijera",tijera.getDescripcionResultado());
	}
	
	
	//LAGARTO GANA/PIERDE/EMPATA COMPARA RESULTADO
	
	@Test
	void testLagartoGanaPapel_comparar_DescripcionResultado(){
		lagarto.comparar(papel);
		assertEquals("lagarto le gana a papel",lagarto.getDescripcionResultado());
	}
	
	@Test
	void testLagartoGanaSpock_comparar_DescripcionResultado(){
		lagarto.comparar(spock);
		assertEquals("lagarto le gana a spock",lagarto.getDescripcionResultado());
	}

	@Test
	void testLagartoPierdePiedra_comparar_DescripcionResultado(){
		lagarto.comparar(piedra);
		assertEquals("lagarto pierde con piedra",lagarto.getDescripcionResultado());
	}
	
	@Test
	void testLagartoPierdeTijera_comparar_DescripcionResultado(){
		lagarto.comparar(tijera);
		assertEquals("lagarto pierde con tijera",lagarto.getDescripcionResultado());
	}

	@Test
	void testLagartoEmpataLagarto_comparar_DescripcionResultado(){
		lagarto.comparar(lagarto);
		assertEquals("lagarto empata con lagarto",lagarto.getDescripcionResultado());
	}

	//SPOCK GANA/PIERDE/EMPATA COMPARA RESULTADO

	@Test
	void testSpockGanaTijera_comparar_DescripcionResultado(){
		spock.comparar(tijera);
		assertEquals("spock le gana a tijera",spock.getDescripcionResultado());
	}

	@Test
	void testSpockGanaPiedra_comparar_DescripcionResultado(){
		spock.comparar(piedra);
		assertEquals("spock le gana a piedra",spock.getDescripcionResultado());
	}

	@Test
	void testSpockPierdePapel_comparar_DescripcionResultado(){
		spock.comparar(papel);
		assertEquals("spock pierde con papel",spock.getDescripcionResultado());
	}

	@Test
	void testSpockPierdeLagarto_comparar_DescripcionResultado(){
		spock.comparar(lagarto);
		assertEquals("spock pierde con lagarto",spock.getDescripcionResultado());
	}

	@Test
	void testSpockEmpataSpock_comparar_DescripcionResultado(){
		spock.comparar(spock);
		assertEquals("spock empata con spock",spock.getDescripcionResultado());
	}


	

	//GET INSTANCE

	@Test
	void testGetInstance_piedra() {
		assertEquals("piedra",PiedraPapelTijeraLagartoSpockFactory.getInstance(PiedraPapelTijeraLagartoSpockFactory.PIEDRA).getNombre());

	}

	@Test
	void testGetInstance_papel() {
		assertEquals("papel",PiedraPapelTijeraLagartoSpockFactory.getInstance(PiedraPapelTijeraLagartoSpockFactory.PAPEL).getNombre());

	}

	@Test
	void testGetInstance_tijera() {
		assertEquals("tijera",PiedraPapelTijeraLagartoSpockFactory.getInstance(PiedraPapelTijeraLagartoSpockFactory.TIJERA).getNombre());

	}

	@Test
	void testGetInstance_lagarto() {
		assertEquals("lagarto",PiedraPapelTijeraLagartoSpockFactory.getInstance(PiedraPapelTijeraLagartoSpockFactory.LAGARTO).getNombre());

	}

	@Test
	void testGetInstance_spock() {
		assertEquals("spock",PiedraPapelTijeraLagartoSpockFactory.getInstance(PiedraPapelTijeraLagartoSpockFactory.SPOCK).getNombre());

	}
}
