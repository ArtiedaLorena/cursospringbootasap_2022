package ar.edu.asap.practica00.modelo;

public class Tijera extends PiedraPapelTijeraLagartoSpockFactory {

	public Tijera() {
		this("tijera", PiedraPapelTijeraLagartoSpockFactory.TIJERA);
	}

	public Tijera(String nombre, int numero) {
		super(nombre, numero);

	}

	@Override
	public boolean isMe(int pNUm) {

		return pNUm == PiedraPapelTijeraLagartoSpockFactory.TIJERA;
	}

	/*
	@Override
	public int comparar(PiedraPapelTijeraLagartoSpockFactory pPPTFact) {
		int result = 0;

		switch (pPPTFact.getNumero()) {
		case PiedraPapelTijeraLagartoSpockFactory.PAPEL:
			result = 1;
			this.descripcionResultado = nombre + " le gana a " + pPPTFact.getNombre();

			break;

		case PiedraPapelTijeraLagartoSpockFactory.PIEDRA:
			result = -1;
			this.descripcionResultado = nombre + " pierde con " + pPPTFact.getNombre();

			break;
		default:

			this.descripcionResultado = nombre + " empata con " + pPPTFact.getNombre();

			break;

		}
		return result;
	}
	*/
	
	@Override
	public int comparar(PiedraPapelTijeraLagartoSpockFactory pPPTFact) {
		int result = 0;

		switch (pPPTFact.getNumero()) {
		
		//TIJERA LE GANA A PAPEL
		case PiedraPapelTijeraLagartoSpockFactory.PAPEL:
			result = 1;
			this.descripcionResultado = nombre + " le gana a " + pPPTFact.getNombre();

			break;
			
		//TIJERA LE GANA A LAGARTO
			
		case PiedraPapelTijeraLagartoSpockFactory.LAGARTO:
			result = 2;
			this.descripcionResultado = nombre + " le gana a " + pPPTFact.getNombre();

			break;
		
		//TIJERA PIERDE CON PIEDRA
		case PiedraPapelTijeraLagartoSpockFactory.PIEDRA:
			result = -3;
			this.descripcionResultado = nombre + " pierde con " + pPPTFact.getNombre();

			break;
		//TIJERA PIERDE CON SPOCK	
		case PiedraPapelTijeraLagartoSpockFactory.SPOCK:
			result = -1;
			this.descripcionResultado = nombre + " pierde con " + pPPTFact.getNombre();

			break;
			
		//TIJERA EMPATA CON TIJERA
		default:

			this.descripcionResultado = nombre + " empata con " + pPPTFact.getNombre();

			break;

		}
		return result;
	}


}
