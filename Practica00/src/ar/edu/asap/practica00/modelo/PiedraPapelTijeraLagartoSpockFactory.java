package ar.edu.asap.practica00.modelo;

import java.util.ArrayList;
import java.util.List;

public abstract class PiedraPapelTijeraLagartoSpockFactory {
	// Control O para ver si vamos bien con las variables
	public static final int PIEDRA = 1;

	public static final int PAPEL = 2;

	public static final int TIJERA = 3;
	
	public static final int SPOCK= 4;
	
	public static final int LAGARTO = 5;

	protected String descripcionResultado;

	private static List<PiedraPapelTijeraLagartoSpockFactory> elementos;

	

	protected String nombre;

	protected int numero;

	public PiedraPapelTijeraLagartoSpockFactory(String nombre, int numero) {
		super();
		this.nombre = nombre;
		this.numero = numero;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getDescripcionResultado() {
		return descripcionResultado;
	}

	public abstract boolean isMe(int pNUm);

	public abstract int comparar(PiedraPapelTijeraLagartoSpockFactory pPPTFactorty);

	public static PiedraPapelTijeraLagartoSpockFactory getInstance(int pNUm) {
		// El padre conoce a todos sus hijos
		// Al todo Se accede mediante window -> show view -> Task
		elementos = new ArrayList<PiedraPapelTijeraLagartoSpockFactory>();
		elementos.add(new Piedra());
		elementos.add(new Papel());
		elementos.add(new Tijera());
		elementos.add(new Lagarto());
		elementos.add(new Spock());

		for (PiedraPapelTijeraLagartoSpockFactory piedraPapelTijeraLagartoSpockFactory : elementos) {
			if (piedraPapelTijeraLagartoSpockFactory.isMe(pNUm))
				return piedraPapelTijeraLagartoSpockFactory;
		}

		return null;
		

	}

	@Override
	public String toString() {
		return getNumero()+ "-"+ getNombre();
	}
	
	
	

}
