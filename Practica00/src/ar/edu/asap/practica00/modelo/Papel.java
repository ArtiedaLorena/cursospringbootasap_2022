package ar.edu.asap.practica00.modelo;

public class Papel extends PiedraPapelTijeraLagartoSpockFactory {

	public Papel() {
		this("papel", PiedraPapelTijeraLagartoSpockFactory.PAPEL);
	}

	public Papel(String nombre, int numero) {
		super(nombre, numero);

	}

	@Override
	public boolean isMe(int pNUm) {

		return pNUm == PiedraPapelTijeraLagartoSpockFactory.PAPEL;
	}

	/*@Override
	public int comparar(PiedraPapelTijeraFactory pPPTFact) {
		int result = 0;

		switch (pPPTFact.getNumero()) {
		case PiedraPapelTijeraFactory.PIEDRA:
			result = 1;
			this.descripcionResultado = nombre + "le gana a " + pPPTFact.getNombre();

			break;

		case PiedraPapelTijeraFactory.TIJERA:
			result = -1;
			this.descripcionResultado = nombre + "Pierde con " + pPPTFact.getNombre();

			break;
		
			default:

			this.descripcionResultado = nombre + "empata " + pPPTFact.getNombre();

			break;

		}
		return result;
	}
	*/
	
	@Override
	public int comparar(PiedraPapelTijeraLagartoSpockFactory pPPTFact) {
		int result = 0;

		switch (pPPTFact.getNumero()) {
		
		//PAPEL LE GANA A PIEDRA
		case PiedraPapelTijeraLagartoSpockFactory.PIEDRA:
			result = 1;
			this.descripcionResultado = nombre + " le gana a " + pPPTFact.getNombre();

			break;
		//PAPEL LE GANA A SPOCK
		case PiedraPapelTijeraLagartoSpockFactory.SPOCK:	
			result = 2;
			this.descripcionResultado = nombre + " le gana a " + pPPTFact.getNombre();

			break;
		//PAPEL PIERDE CON TIJERA
		case PiedraPapelTijeraLagartoSpockFactory.TIJERA:
			result = -3;
			this.descripcionResultado = nombre + " pierde con " + pPPTFact.getNombre();

			break;
		//PAPEL PIERDE CON LAGARTO	
		case PiedraPapelTijeraLagartoSpockFactory.LAGARTO:
			result = -1;
			this.descripcionResultado = nombre + " pierde con " + pPPTFact.getNombre();

			break;
			
		//PAPEL EMPATA CON PAPEL	
		default:

			this.descripcionResultado = nombre + " empata con " + pPPTFact.getNombre();

			break;

		}
		return result;
	}

}
