package ar.edu.asap.practica00.modelo;

public class Lagarto extends PiedraPapelTijeraLagartoSpockFactory {
	
	public Lagarto() {
		this("lagarto", PiedraPapelTijeraLagartoSpockFactory.LAGARTO);
	}

	public Lagarto(String nombre, int numero) {
		super(nombre, numero);
		
	}

	@Override
	public boolean isMe(int pNUm) {

		return pNUm == PiedraPapelTijeraLagartoSpockFactory.LAGARTO;
	}

	@Override
	public int comparar(PiedraPapelTijeraLagartoSpockFactory pPPTFact) {
		int result = 0;

		switch (pPPTFact.getNumero()) {
		
		//LAGARTO LE GANA A PAPEL
		case PiedraPapelTijeraLagartoSpockFactory.PAPEL:
			result = 1;
			this.descripcionResultado = nombre + " le gana a " + pPPTFact.getNombre();

			break;
			
		//LAGARTO LE GANA A SPOCK
			
		case PiedraPapelTijeraLagartoSpockFactory.SPOCK:
			result = 2;
			this.descripcionResultado = nombre + " le gana a " + pPPTFact.getNombre();

			break;
		
		//LAGARTO PIERDE CON PIEDRA
		case PiedraPapelTijeraLagartoSpockFactory.PIEDRA:
			result = -3;
			this.descripcionResultado = nombre + " pierde con " + pPPTFact.getNombre();

			break;
		//LAGARTO PIERDE CON TIJERA	
		case PiedraPapelTijeraLagartoSpockFactory.TIJERA:
			result = -1;
			this.descripcionResultado = nombre + " pierde con " + pPPTFact.getNombre();

			break;
			
		//LAGARTO EMPATA CON LAGARTO
		default:

			this.descripcionResultado = nombre + " empata con " + pPPTFact.getNombre();

			break;

		}
		return result;
	}


}
