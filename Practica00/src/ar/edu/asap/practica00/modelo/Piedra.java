package ar.edu.asap.practica00.modelo;

public class Piedra extends PiedraPapelTijeraLagartoSpockFactory {

	public Piedra() {
		this("piedra", PiedraPapelTijeraLagartoSpockFactory.PIEDRA);
	}

	public Piedra(String nombre, int numero) {
		super(nombre, numero);

	}

	@Override
	public boolean isMe(int pNUm) {

		return pNUm == PiedraPapelTijeraLagartoSpockFactory.PIEDRA;
	}

	@Override
	public int comparar(PiedraPapelTijeraLagartoSpockFactory pPPTFact) {
		int result = 0;

		switch (pPPTFact.getNumero()) {
		
		//PIEDRA LE GANA A TIJERA
		case PiedraPapelTijeraLagartoSpockFactory.TIJERA:
			result = 1;
			this.descripcionResultado = nombre + " le gana a " + pPPTFact.getNombre();

			break;
		//PIEDRA LE GANA A LAGARTO
		case PiedraPapelTijeraLagartoSpockFactory.LAGARTO:	
			result = 2;
			this.descripcionResultado = nombre + " le gana a " + pPPTFact.getNombre();

			break;
		//PIEDRA PIERDE CON PAPEL
		case PiedraPapelTijeraLagartoSpockFactory.PAPEL:
			result = -3;
			this.descripcionResultado = nombre + " pierde con " + pPPTFact.getNombre();

			break;
		//PIEDRA PIERDE CON SPOCK	
		case PiedraPapelTijeraLagartoSpockFactory.SPOCK:
			result = -1;
			this.descripcionResultado = nombre + " pierde con " + pPPTFact.getNombre();

			break;
			
		//PIEDRA EMPATA CON PIEDRA	
		default:

			this.descripcionResultado = nombre + " empata con " + pPPTFact.getNombre();

			break;

		}
		return result;
	}

}
