package com.domain.controller;
import ar.edu.asap.practica00.modelo.PiedraPapelTijeraLagartoSpockFactory;
import java.util.ArrayList;
import java.util.List;

import org.apache.jasper.tagplugins.jstl.core.ForEach;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ar.edu.asap.practica00.modelo.Piedra;
import ar.edu.asap.practica00.modelo.PiedraPapelTijeraLagartoSpockFactory;
import ar.edu.asap.practica00.modelo.*;


@Controller
public class IndexController {
	
	

	@RequestMapping("/home")
	public String  goIndex() {
		return "Index";
	}
	
	@RequestMapping("/")
	public String  goPresentacion() {
		return "Presentacion";
	}
	

	@RequestMapping("/listado")
	public String goListado(Model model) {
		List<String> alumnos = new ArrayList<String>();
		alumnos.add("Juan");
		alumnos.add("Pedro");
		alumnos.add("Jos�");
		model.addAttribute("titulo", "Listado de alumnos");
		model.addAttribute("profesor", "Gabriel Casas");
		model.addAttribute("alumnos", alumnos);
		return "Listado";
	}
	
	@RequestMapping("/juego")
	public String goListadoJuego(Model model) {
		
	List<PiedraPapelTijeraLagartoSpockFactory> opciones = new ArrayList<PiedraPapelTijeraLagartoSpockFactory>();
	
	
	opciones.add(new Piedra());
	opciones.add(new Papel());
	opciones.add(new Tijera());
	opciones.add(new Lagarto());
	opciones.add(new Spock());
	
	
	
	
	model.addAttribute("opciones", opciones);
	
	
	
	return "PiedraPapelTijera";
	}
	
	@RequestMapping("/resolverJuego")
	public String goResultado(@RequestParam(required = false) Integer selOpcion, Model model)
 {
	System.out.println(selOpcion);
	PiedraPapelTijeraLagartoSpockFactory computadora= PiedraPapelTijeraLagartoSpockFactory.getInstance((int)(Math.random()*5+1));
	PiedraPapelTijeraLagartoSpockFactory jugador= PiedraPapelTijeraLagartoSpockFactory.getInstance(selOpcion.intValue());
	
	int result= jugador.comparar(computadora);
	
	model.addAttribute("numero", selOpcion);
	model.addAttribute("jugador",jugador);
	model.addAttribute("computadora",computadora);
	model.addAttribute("resultado", jugador.getDescripcionResultado());
	
	
	
	
		
	return "Resultados";
	}
	
	public IndexController() {
		
	}
}
